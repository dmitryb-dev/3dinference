name := "3DInference"

version := "1.0"

libraryDependencies += "io.reactivex" %% "rxscala" % "0.26.5"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test"

scalaVersion := "2.12.3"
        