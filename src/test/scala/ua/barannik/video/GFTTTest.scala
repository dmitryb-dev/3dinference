package ua.barannik.video

import ua.barannik.transform.{Color, OpticalFlow}
import ua.barannik.video.FileVideoSource._
import ua.barannik.video.FramesStream.ObservableTransformer

object GFTTTest extends App {
  val viewer1 = new MatViewer("Video1")
  val viewer2 = new MatViewer("Video2", offset = Point(1, 0))
  "Adele.mp4".frames
    .skip(1000)
    .map(Color.toGray)
    .pairs
    .foreach { case (f, s) =>
      viewer1 view f
      viewer2 viewWithKeyPoints (f, OpticalFlow.goodPointsToTrack(f))
    }
}
