package ua.barannik.video

import ua.barannik.reconstruction.{DepthMapReconstruction, OpticalFlowReconstruction, PLY}
import ua.barannik.transform.OpticalShift._
import ua.barannik.transform.{Color, OpticalFlow, OpticalShift}
import ua.barannik.video.FileVideoSource._
import ua.barannik.video.FramesStream.ObservableTransformer

import scala.collection.mutable.ArrayBuffer

object DepthReconstructionTest extends App {
  val viewer1 = new MatViewer("Video1")
  val viewer2 = new MatViewer("Video2", offset = Point(1, 0))

  var savedFeatures = null

  "Adele.mp4".frames
    .skip(1000)
   // .emitEvery(2)
    .map(Color.toGray)
    .pairs
    .map { case (f, s) =>
      val features = OpticalFlow.goodFeaturesToTrack(f)

      val flow: ArrayBuffer[OpticalShift] = (OpticalFlow.LKOpticalFlow(f, s, features), features)
      (f, flow)
    }
    .map { case (f, flow) => (f, OpticalFlow.filterFlow(flow)) }
    .map { case (f, flow) => (f, OpticalFlow.onlyMovingObject(flow)) }
    .foreach { case (f, flow) =>
      val (prev, next) = OpticalShift2Mat(flow)
      viewer1 view f
      val points = DepthMapReconstruction.reconstruct(flow)
    //  viewer2 viewWithDepthMap(f, DepthMapReconstruction.reduceNoise(points))
      viewer2 viewWithDepthMap(f, points)

        Thread.sleep(3000)
        new PLY().save(DepthMapReconstruction.zScale(points, 50))
    }
}
