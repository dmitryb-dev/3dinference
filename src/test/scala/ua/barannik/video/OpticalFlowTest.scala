package ua.barannik.video

import java.util

import org.bytedeco.javacpp.opencv_core.Mat
import ua.barannik.transform.{Color, OpticalFlow}
import ua.barannik.video.FileVideoSource._
import ua.barannik.video.FramesStream.ObservableTransformer
import ua.barannik.util.MatConversions._

object OpticalFlowTest extends App {
  val viewer1 = new MatViewer("Video1")
  val viewer2 = new MatViewer("Video2", offset = Point(1, 0))
  "Adele.mp4".frames
    .skip(1000)
    .map(Color.toGray)
    .pairs
    .foreach { case (f, s) =>
      val features = OpticalFlow.goodFeaturesToTrack(f)
      val flow = OpticalFlow.LKOpticalFlow(f, s, features)
      viewer1 view f
      viewer2 viewWithOpticalFlow(s, MatViewer.asMatArray(features), MatViewer.asMatArray(flow))
    }
}
