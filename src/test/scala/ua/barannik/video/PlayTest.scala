package ua.barannik.video

import ua.barannik.video.FileVideoSource._
import ua.barannik.video.FramesStream.ObservableTransformer

object PlayTest extends App {
  val viewer1 = new MatViewer("Video1")
  val viewer2 = new MatViewer("Video2", offset = Point(1, 0))
  "Adele.mp4".frames.emitEvery(130).pairs
    .foreach { case (f, s) =>
        viewer1 view f
        viewer2 view s
        Thread.sleep(100)
    }
}
