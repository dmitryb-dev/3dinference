package ua.barannik.video

import org.scalatest.FlatSpec
import ua.barannik.video.FileVideoSource.fromFilePathString

class FileVideoSourceTest extends FlatSpec {
  it should "Implicitly converts from string" in {
    assert("video.mp4".filepath == "video.mp4")
  }
}
