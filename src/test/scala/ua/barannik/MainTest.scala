package ua.barannik

import ua.barannik.video.FileVideoSource._
import ua.barannik.video.{MatViewer, Point}
import ua.barannik.video.FramesStream.ObservableTransformer

object MainTest extends App {
  val viewer1 = new MatViewer("Video1")
  val viewer2 = new MatViewer("Video2", offset = Point(1, 0))
  "Adele.mp4".frames.pairs
    .foreach { case (f, s) =>
        viewer1 view f
        viewer2 view s
    }
}
