package ua.barannik

package object video {
  case class Point[T](x: T, y: T)
}
