package ua.barannik.video

import java.awt.Color
import java.awt.image.BufferedImage
import java.nio.FloatBuffer
import javax.swing.{ImageIcon, JFrame, JLabel}

import org.bytedeco.javacpp.opencv_core.{KeyPointVector, Mat}
import org.bytedeco.javacpp.opencv_features2d._
import org.bytedeco.javacv.{Frame, Java2DFrameConverter, OpenCVFrameConverter}
import ua.barannik.reconstruction.Point3D
import ua.barannik.video.MatViewer._

import scala.reflect.ClassTag


class MatViewer(val title: String,
                size: Point[Int] = Point(500, 500),
                offset: Point[Int] = Point(0, 0)) {

  private[this] val jFrame: JFrame = new JFrame(title)
  jFrame.setBounds(offset.x * size.x, offset.y * size.y, size.x, size.y)
  jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)

  private[this] val vidPanel: JLabel = new JLabel()
  jFrame.setContentPane(vidPanel)

  show()

  def view(bfImage: BufferedImage) = {
    val image = new ImageIcon(bfImage)
    vidPanel.setIcon(image)
    vidPanel.repaint()
  }

  def view(frame: Mat): Unit = {
    view(toBufferedImage(frame))
  }

  def viewWithKeyPoints(mat: Mat, points: KeyPointVector) = {
    val result = new Mat()
    drawKeypoints(mat, points, result)
    view(result)
  }

  def viewWithOpticalFlow(mat: Mat, prevPoints: MatArray[Float], newPoints: MatArray[Float]) = {
    val image = toBufferedImage(mat)
    val graphics = image.createGraphics()
    for (c <- 0 until prevPoints.columns;
         r <- 0 until prevPoints.rows) {
      graphics.drawLine(
        prevPoints.get(0, c, r) toInt, prevPoints.get(1, c, r) toInt,
        newPoints.get(0, c, r) toInt, newPoints.get(1, c, r) toInt)
    }
    graphics.dispose()
    view(image)
  }

  def viewWithDepthMap(mat: Mat, points: Seq[Point3D]) = {
    val image = toBufferedImage(mat)
    val graphics = image.createGraphics()
    for (point <- points) {
      val color = (255 * point.z).toInt
      graphics.setColor(new Color(color, color, color))
      graphics.fillOval(point.x.toInt, point.y.toInt, 5, 5)
    }
    graphics.dispose()
    view(image)
  }

  def show() = jFrame.setVisible(true)
  def close() = jFrame.setVisible(false)
}

object MatViewer {
  class MatArray[T : ClassTag](val channels: Int, val columns: Int, val rows: Int) {
    val array = new Array[Array[Array[T]]] (channels)
    for (ch <- 0 until channels;
         c <- 0 until columns) {
      array(ch) = new Array[Array[T]] (columns)
      array(ch)(c) = new Array[T] (rows)
    }

    def get(ch: Int, col: Int, row: Int): T = array(ch)(col)(row)
    def update(ch: Int, col: Int, row: Int, value: T) = array(ch)(col)(row) = value

    override def toString: String = {
      val builder = new StringBuilder()
      for (r <- 0 until rows) {
        for (c <- 0 until columns) {
          val chValue = (
            for (ch <- 0 until channels)
              yield get(ch, c, r)
            ).mkString(",").format("%20s")
          builder append chValue
        }
        builder append "\n"
      }
      builder toString()
    }
  }

  def toBufferedImage(mat: Mat) = {
    val toFrame = (frame: Mat) => new OpenCVFrameConverter.ToMat().convert(frame)
    val toBufferedImage = (frame: Frame) => new Java2DFrameConverter().convert(frame)
    val convert = toFrame andThen toBufferedImage

    convert(mat)
  }

  implicit def asMatArray(mat: Mat) = {
    val buffer = mat.createBuffer[FloatBuffer]()
    val array = new MatArray[Float](mat.channels(), mat.cols(), mat.rows())
    for (c <- 0 until mat.cols();
         r <- 0 until mat.rows();
         ch <- 0 until mat.channels())
      array(ch, c, r) = buffer.get()
    array
  }
}
