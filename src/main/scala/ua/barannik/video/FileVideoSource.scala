package ua.barannik.video

import org.bytedeco.javacpp.opencv_core.Mat
import org.bytedeco.javacpp.opencv_videoio.VideoCapture
import rx.lang.scala.Observable
import ua.barannik.video.FileVideoSource.async

class FileVideoSource(path: String) extends VideoSource {
  val filepath =
    if (path startsWith "/")
      path
    else {
      val resource = getClass().getClassLoader().getResource(path)
      require(resource != null, s"No such resource: $path")
      resource.getPath
    }

  override def frames: Observable[Mat] = {
    val capture = new VideoCapture(filepath)

    Observable(subscriber =>
      async {
        var frame = new Mat()
        while(capture.read(frame)) {
          if (!subscriber.isUnsubscribed)
            subscriber.onNext(frame)
          frame = new Mat()
        }
        if (!subscriber.isUnsubscribed)
          subscriber.onCompleted()
      }
    )
  }
}

object FileVideoSource {
  implicit def fromFilePathString(filePath: String): FileVideoSource = new FileVideoSource(filePath)
  private def async(action: => Unit) = new Thread(() => action).start()
}