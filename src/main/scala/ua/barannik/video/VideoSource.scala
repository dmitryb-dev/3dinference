package ua.barannik.video

import org.bytedeco.javacpp.opencv_core.Mat
import rx.lang.scala.Observable

trait VideoSource {
  def frames: Observable[Mat]
}
