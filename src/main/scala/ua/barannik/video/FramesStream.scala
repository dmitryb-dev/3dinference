package ua.barannik.video

import rx.lang.scala.Observable

object FramesStream {
  implicit class ObservableTransformer[T](observable: Observable[T]) {

    def pairs: Observable[(T, T)] = Observable(subscriber => {
      var prev: Option[T] = None
      observable.subscribe(next => {
        prev match {
          case Some(prevValue) => subscriber.onNext(prevValue, next)
          case _ =>
        }
        prev = Option(next)
      })
    })

    def emitEvery(skippingFrames: Int): Observable[T] = {
      var frameNumber = 0
      observable.filter { _ =>
        frameNumber = frameNumber + 1
        frameNumber % skippingFrames == 0
      }
    }

    def skip(toSkip: Int): Observable[T] = {
      var counter = -1
      observable.filter { _ =>
        counter = counter + 1
        counter > toSkip
      }
    }
  }
}
