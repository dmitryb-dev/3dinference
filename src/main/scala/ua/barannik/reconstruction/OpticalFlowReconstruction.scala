package ua.barannik.reconstruction

import ua.barannik.transform.OpticalShift

import scala.collection.mutable.ArrayBuffer

object OpticalFlowReconstruction {
  def reconstruct(shift: Seq[OpticalShift]) = {
    val result = new ArrayBuffer[Point3D]

    for (shiftPoint <- shift)
      yield flowTo3DPosition(shiftPoint)
  }

  def flowTo3DPosition(opticalShift: OpticalShift) = {
    val delta = opticalShift.length
    val B = 1f
    val f = 300f
    val x = opticalShift.prev.x
    val y = opticalShift.prev.y
    val w = 500f
    val h = 400f

  //  val Z = (B * f) / delta
    val Z = 1
    val X = (Z * (x - w / 2f)) / f
    val Y = (Z * (y - h / 2f)) / f

    Point3D(X.toFloat, Y.toFloat, Z.toFloat)
  }

}
