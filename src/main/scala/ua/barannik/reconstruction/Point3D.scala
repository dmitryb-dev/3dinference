package ua.barannik.reconstruction

import java.lang.{Float => jFloat}

case class Point3D(x: jFloat, y: jFloat, z: jFloat) {
  override def toString: String = String.format("%f %f %f", x, y, z)
}
