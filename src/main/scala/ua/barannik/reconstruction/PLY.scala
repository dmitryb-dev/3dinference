package ua.barannik.reconstruction

import java.io.{File, PrintWriter}

class PLY(val filepath: String = "/Users/dmitry/Desktop/output/") {
    def asPlyString(vertexes: Seq[Point3D]) = {
    ("ply\nformat ascii 1.0\n" +
      "element vertex %d\n" +
      "property float x\n" +
      "property float y\n" +
      "property float z\n" +
      "end_header\n" +
      "%s")
        .format(vertexes.length, vertexes.mkString("\n")) + "\n"
  }

  def save(vertexes: Seq[Point3D]) = {
    val f = new File(filepath + "frame" + PLY.counter + ".ply")
    new PrintWriter(f) {
      write(asPlyString(vertexes))
      close()
    }
    PLY.counter = PLY.counter + 1
  }
}

object PLY {
  var counter = 0
}
