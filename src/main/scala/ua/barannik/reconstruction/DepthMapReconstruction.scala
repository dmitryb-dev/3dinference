package ua.barannik.reconstruction

import java.nio.{ByteBuffer, FloatBuffer}

import org.bytedeco.javacpp.opencv_core.{CV_8U, Mat}
import ua.barannik.transform.{Noise, OpticalShift}

object DepthMapReconstruction {

  def reduceNoise(points: Seq[Point3D]) = {
    val noised = new Mat(1, points.length, CV_8U)
    val noisedBuffer = noised.createBuffer[ByteBuffer]()

    for (p <- points) {
      noisedBuffer.put((p.z * 255).toByte)
    }

    val smoothed = new Array[Point3D](points.length)
    val denoised = Noise.reduce(noised)
    val denoisedBuffer = denoised.createBuffer[ByteBuffer]()

    for (i <- 0 until points.length) {
      smoothed(i) = Point3D(points(i).x, points(i).y, (denoisedBuffer.get() & 0xFF) / 255f)
    }

    smoothed
  }

  def gaussian(points: Seq[Point3D]) = {
    val windowSize = 4
    val smoothed = new Array[Point3D](points.length - windowSize)
    for (i <- 0 until (points.length - windowSize)) {
      var sum = 0f
      for (j <- 0 until windowSize) {
        sum = sum + points(i + j).z
      }
      smoothed(i) = Point3D(
        points(i + windowSize / 2).x,
        points(i + windowSize / 2).y,
        sum / windowSize
      )
    }
    smoothed
  }

  def zScale(points: Seq[Point3D], power: Float) = {
    for (p <- points)
      yield Point3D(p.x, p.y, p.z * power)
  }

  def reduce(points: Seq[Point3D]) = {
    val windowSize = 3
    val smoothed = new Array[Point3D](points.length - windowSize)
    for (i <- 0 until (points.length - windowSize) by windowSize) {
      var min = 1f
      for (j <- 0 until windowSize if points(i + j).z < min) {
        min = points(i + j).z
      }
      smoothed(i) = Point3D(
        points(i + windowSize / 2).x,
        points(i + windowSize / 2).y,
        min / windowSize
      )
    }
    smoothed
  }

  def reconstruct(shift: Seq[OpticalShift]) = {
    implicit val ordering = Ordering.by[OpticalShift, Double](shift => shift.length)
    for (shiftPoint <- shift)
      yield new Point3D(shiftPoint.prev.x, shiftPoint.prev.y, Math.max((shiftPoint.length / shift.max.length).toFloat * 2 - 1, 0))
  }
}
