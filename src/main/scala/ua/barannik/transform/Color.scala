package ua.barannik.transform

import org.bytedeco.javacpp.opencv_core.Mat
import org.bytedeco.javacpp.opencv_imgproc._

object Color {
  def toGray(mat: Mat): Mat = {
    val grayed = new Mat()
    cvtColor(mat, grayed, COLOR_RGB2GRAY)
    grayed
  }
}
