package ua.barannik.transform

import java.nio.FloatBuffer

import org.bytedeco.javacpp.opencv_core.Mat
import ua.barannik.video.MatViewer.MatArray

import scala.collection.mutable.ArrayBuffer

case class Point(x: Float, y: Float)

case class OpticalShift(prev: Point, next: Point) {
  lazy val length = { Math.sqrt(Math.pow(prev.x - next.x, 2) + Math.pow(prev.y - next.y, 2)) }
}

object OpticalShift {

  implicit def Mat2OpticalShiftBuffer(pair: (Mat, Mat)): ArrayBuffer[OpticalShift] = {
    val buffer = new ArrayBuffer[OpticalShift]()
    buffer.appendAll(Mat2OpticalShift(pair))
    buffer
  }

  implicit def Mat2OpticalShift(pair: (Mat, Mat)): Array[OpticalShift] = {
    val (prev, next) = pair
    Mat2OpticalShift(prev, next)
  }
  implicit def Mat2OpticalShift(prev: Mat, next: Mat): Array[OpticalShift] = {
    val prevBuffer = prev.createBuffer[FloatBuffer]()
    val nextBuffer = next.createBuffer[FloatBuffer]()

    val array = new Array[OpticalShift](prev.rows())
    for (i <- 0 until array.length)
      array(i) = OpticalShift(
        Point(prevBuffer.get(), prevBuffer.get()),
        Point(nextBuffer.get(), nextBuffer.get())
      )
    array
  }

  implicit def OpticalShift2Mat(shift: Seq[OpticalShift]): (Mat, Mat) = {
    val prev, next = new Mat(shift.length, 1, 13)
    val prevBuffer = prev.createBuffer[FloatBuffer]()
    val nextBuffer = next.createBuffer[FloatBuffer]()

    for (os <- shift) {
      prevBuffer.put(os.prev.x)
      prevBuffer.put(os.prev.y)
      nextBuffer.put(os.next.x)
      nextBuffer.put(os.next.y)
    }
    (prev, next)
  }
}
