package ua.barannik.transform

import java.util

import org.bytedeco.javacpp.opencv_core._
import org.bytedeco.javacpp.opencv_video._
import org.bytedeco.javacpp.opencv_features2d._
import org.bytedeco.javacpp.opencv_imgproc

import scala.collection.mutable.ArrayBuffer

object OpticalFlow {
  def goodPointsToTrack(grayedImage: Mat,
                        maxCount: Int = 1000,
                        precision: Double = 0.001,
                        maxDistance: Int = 30) = {

    val gftt = GFTTDetector.create(maxCount, precision, 10.0, maxDistance, false, 0.04)
    val points = new KeyPointVector()
    gftt.detect(grayedImage, points)
    points
  }

  def goodFeaturesToTrack(grayedImage: Mat,
                          maxCount: Int = 1000,
                          precision: Double = 0.001) = {
    val points = new Mat()
    opencv_imgproc.goodFeaturesToTrack(grayedImage, points, maxCount, precision, 10.0)
    points
  }

  def LKOpticalFlow(prev: Mat, next: Mat, points: Mat) = {
    val newPoints = new Mat()
    calcOpticalFlowPyrLK(prev, next, points, newPoints, new Mat(), new Mat())
    newPoints
  }

  def pyramidFrameFor(mat: Mat) = {
    val pyramid = new MatVector()
    buildOpticalFlowPyramid(mat, pyramid, new Size(10), 5)
    pyramid
  }

  def filterFlow(opticalShift: ArrayBuffer[OpticalShift]) = {
    val average = opticalShift.foldRight(0d)(_.length + _) / opticalShift.length
    val threshold = average * 3

    for (i <- opticalShift.length - 1 to 0 by -1;
         shift = opticalShift(i)
         if shift.length > threshold)
      opticalShift.remove(i)

    opticalShift
  }

  def onlyMovingObject(opticalShift: ArrayBuffer[OpticalShift]) = {
    val average = opticalShift.foldRight(0d)(_.length + _) / opticalShift.length

    for (i <- opticalShift.length - 1 to 0 by -1;
         shift = opticalShift(i)
         if shift.length < average)
      opticalShift.remove(i)

    opticalShift
  }
}
