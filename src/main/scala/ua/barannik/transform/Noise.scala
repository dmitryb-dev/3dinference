package ua.barannik.transform

import org.bytedeco.javacpp.opencv_core.Mat
import org.bytedeco.javacpp.opencv_core.CV_8U
import org.bytedeco.javacpp.opencv_photo.fastNlMeansDenoising

object Noise {

  def reduce(mat: Mat) = {
    val result = new Mat(mat.rows(), mat.cols(), CV_8U)
    fastNlMeansDenoising(mat, result)
    result
  }

}
