package ua.barannik.util

import java.nio.FloatBuffer
import java.util

import org.bytedeco.javacpp.opencv_core.Mat

import scala.collection.mutable.ArrayBuffer

object MatConversions {
  def Mat2Array(mat: Mat): ArrayBuffer[Float] = {
    val length = mat.rows() * mat.cols() * mat.channels()
    val array = new ArrayBuffer[Float](length)
    val matBuffer = mat.createBuffer[FloatBuffer]()
    for (_ <- 1 to length)
      array.append(matBuffer.get())
    array
  }
  def Array2Mat(array: ArrayBuffer[Float], c: Int, r: Int, t: Int) = {
    val mat = new Mat(r, c, t)
    val matBuffer = mat.createBuffer[FloatBuffer]()
    for (e <- array) matBuffer.put(e)
    mat
  }
}
